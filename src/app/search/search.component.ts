import { Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import { DataService } from '../data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChanged = new EventEmitter();
  @Input() city: string;
  formGroup: FormGroup;

  constructor (public data: DataService,
              private router: Router) {}

  ngOnInit () {
    this.formGroup = new FormGroup ({
      method: new FormControl(this.data.method)
    });
    this.city = this.data.city;
  }

  sendText(value: string): void {
    if (value.length !== 0) {
      this.data.changePage(false);
      this.data.changeSaleOrRent(this.formGroup.value.method);
      this.data.sendCity(value);
      this.onChanged.emit();
      this.router.navigate(['list']);
    }
  }

}
