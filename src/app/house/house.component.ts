import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.css']
})
export class HouseComponent implements OnInit {
  private view = true;

  constructor(private route: ActivatedRoute,
              private data: DataService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.data.id = params.houseId;
    });
    this.view = Boolean(this.data.houses);
  }

}
