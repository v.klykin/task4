import { Component, OnInit} from '@angular/core';
import { DataService } from '../data.service';

interface Ihouse {
  bathroom_number: string;
  bedroom_number: number;
  car_spaces: number;
  commission: number;
  construction_year: number;
  datasource_name: string;
  img_height: number;
  img_url: string;
  img_width: number;
  keywords: string;
  latitude: number;
  lister_name: string;
  lister_url: string;
  listing_type: string;
  location_accuracy: number;
  longitude: number;
  price: number;
  price_currency: string;
  price_formatted: string;
  price_high: number;
  price_low: number;
  price_type: string;
  property_type: string;
  size: number;
  size_type: string;
  summary: string;
  thumb_height: number;
  thumb_url: string;
  thumb_width: number;
  title: string;
  updated_in_days: number;
  updated_in_days_formatted: string;
}
@Component({
  selector: 'app-houses-list',
  templateUrl: './houses-list.component.html',
  styleUrls: ['./houses-list.component.css']
})
export class HousesListComponent implements OnInit {
  constructor(private data: DataService) {
  }


  ngOnInit() {
    this.data.getData(true);
  }

 update(boo: boolean): void {
    this.data.getData(boo);
  }

  incrementPage(flag: boolean): void {
    this.data.changePage(flag);
    this.update(false);
  }
  saveHome(house: Ihouse): void {
    this.data.getHousesLovalStorage();
    if (!this.data.preservedHouses || this.data.preservedHouses.length === 0) {
      this.data.saveHouse(house);
    } else {
      let res = true;
      this.data.preservedHouses.forEach(element => res = element.img_url !== house.img_url);
      if (res) {
        this.data.saveHouse(house);
      }
    }
  }
  goToUp(): void {
    window.scrollTo(0, 0);
  }
}
