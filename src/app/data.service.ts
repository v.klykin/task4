import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Ihouse {
  bathroom_number: string;
  bedroom_number: number;
  car_spaces: number;
  commission: number;
  construction_year: number;
  datasource_name: string;
  img_height: number;
  img_url: string;
  img_width: number;
  keywords: string;
  latitude: number;
  lister_name: string;
  lister_url: string;
  listing_type: string;
  location_accuracy: number;
  longitude: number;
  price: number;
  price_currency: string;
  price_formatted: string;
  price_high: number;
  price_low: number;
  price_type: string;
  property_type: string;
  size: number;
  size_type: string;
  summary: string;
  thumb_height: number;
  thumb_url: string;
  thumb_width: number;
  title: string;
  updated_in_days: number;
  updated_in_days_formatted: string;
}
@Injectable({
  providedIn: 'root'
})



export class DataService {

  constructor(private http: HttpClient) { }
  public city = '';
  public method = 'buy';
  public page = 1;
  public response;
  public title: string;
  public buttonIncrement: boolean;
  public houses: object[];
  public preservedHouses = [];
  public id = '';

  getData(boo: boolean) {
    const url = 'https://api.nestoria.co.uk/api?encoding=json&action=search_listings&listing_type='
      + this.method + '&country=uk&&place_name='
      + this.city + '&page='
      + this.page + '&callback=JSONP_CALLBACK';
      return this.http.jsonp(url, 'callback').subscribe(response => {
          this.response = response;
            if (+(this.response.response.application_response_code) < 200) {
              this.buttonIncrement = true;
              this.title = this.response.response.locations[0].long_title;

              if (this.response.response.listings.length === 0) {
                this.buttonIncrement = false;
                this.title += '  Empty';
              } else {
                this.title += ', Page:' + this.page;
              }
            } else {
              this.buttonIncrement = false;
            }
            if (this.response.response.application_response_text === 'unknown location') {
              this.title = 'Незвестное место!';
            }
            console.log(this.response);
            console.log(this.response.response.listings);
            if (boo && this.page === 1) {
              this.houses = this.response.response.listings;
            } else if (!boo) {
              this.response.response.listings.forEach(item => this.houses.push(item));
            }
          });
  }

  getHousesLovalStorage (): void {
    const houseJson = localStorage.getItem('houses');
    this.preservedHouses = JSON.parse(houseJson);
  }

  sendCity(city: string): void {
    this.city = city;
  }

  changeSaleOrRent(method: string): void {
    this.method = method;
  }

  changePage(increment: boolean): void {
    if (increment) {
      this.page++;

    } else {
      this.page = 1;
    }
  }

  saveHouse(house: Ihouse): void {
    if (!this.preservedHouses) {
      this.preservedHouses = [];
    }
    this.preservedHouses.push(house);
    const houseJson = JSON.stringify(this.preservedHouses);
    localStorage.setItem('houses', houseJson);
  }

  deleteHouse(house: Ihouse): void {
    this.preservedHouses.forEach((element, i) => {
      if (element.img_url === house.img_url) {
        this.preservedHouses.splice(i, 1);
      }
    });
    const houseJson = JSON.stringify(this.preservedHouses);
    localStorage.setItem('houses', houseJson);
  }
}
