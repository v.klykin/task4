import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  {

  constructor (private data: DataService,
              private router: Router) {}

  send (city: string, method: string): void {
    this.data.changePage(false);
    this.data.city = city;
    this.data.changeSaleOrRent(method);
    this.router.navigate(['list']);
  }
}
