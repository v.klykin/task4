import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-save-home',
  templateUrl: './save-home.component.html',
  styleUrls: ['./save-home.component.css']
})
export class SaveHomeComponent implements OnInit {
  private view = true;

  constructor(private route: ActivatedRoute,
              private data: DataService) { }

  ngOnInit() {
    this.data.getHousesLovalStorage();
    this.route.params.subscribe(params => {
      this.data.id = params.houseId;
    });
    this.view = Boolean(this.data.preservedHouses);
  }

}
