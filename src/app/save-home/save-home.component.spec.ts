import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveHomeComponent } from './save-home.component';

describe('SaveHomeComponent', () => {
  let component: SaveHomeComponent;
  let fixture: ComponentFixture<SaveHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
