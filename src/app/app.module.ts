import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataService } from './data.service';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YamapngModule, YaCoreModule } from 'yamapng';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';

import { RetainScrollPolyfillModule } from './retain-scroll-polyfill/retain-scroll-polyfill.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadersComponent } from './headers/headers.component';
import { HousesListComponent } from './houses-list/houses-list.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { MarkerComponent } from './marker/marker.component';
import { HouseComponent } from './house/house.component';
import { SaveHomeComponent } from './save-home/save-home.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    HomeComponent,
    HousesListComponent,
    SearchComponent,
    MarkerComponent,
    HouseComponent,
    SaveHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    ReactiveFormsModule,
    YamapngModule,
    YaCoreModule.forRoot({
      apiKey: '3cbedaac-e1db-4c35-b13a-e7a26578edd0'
    }),
    RetainScrollPolyfillModule.forRoot({
      // Tell the polyfill how long to poll the document after a route change in
      // order to look for elements that need to be restored to a previous offset.
      pollDuration: 3000,
      pollCadence: 50
   }) ],
  entryComponents: [HousesListComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
