import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HousesListComponent } from './houses-list/houses-list.component';
import { HomeComponent } from './home/home.component';
import { MarkerComponent } from './marker/marker.component';
import { HouseComponent } from './house/house.component';
import { SaveHomeComponent } from './save-home/save-home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'list', component: HousesListComponent},
  {path: 'markers', component: MarkerComponent},
  {path: 'list/:houseId', component: HouseComponent},
  {path: 'markers/:houseId', component: SaveHomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
